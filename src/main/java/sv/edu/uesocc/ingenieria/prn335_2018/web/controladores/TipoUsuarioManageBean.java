/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoUsuarioFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoUsuario;

/**
 *
 * @author cristian
 */
@Named(value = "tipoUsuarioManageBean")
@RequestScoped
public class TipoUsuarioManageBean implements Serializable {

    @EJB
    TipoUsuarioFacadeLocal tipoUsrbd;

    private LazyDataModel<TipoUsuario> lazyModelo;
    private TipoUsuario tipoUsuario = new TipoUsuario();
    private List<TipoUsuario> tipoUsuarioList;

    public TipoUsuarioManageBean() {

    }

    public LazyDataModel<TipoUsuario> getLazyModel() {
        return lazyModelo;
    }

    public void setModel(LazyDataModel<TipoUsuario> lazyModel) {
        this.lazyModelo = lazyModel;
    }

    @PostConstruct
    public void post() {
        iniciarTipoUsuario();
        LDM();

    }

    public void LDM() {

        try {
            this.lazyModelo = new LazyDataModel<TipoUsuario>() {

                @Override
                public Object getRowKey(TipoUsuario object) {
                    if (object != null) {
                        return object.getIdTipoUsuario();
                    }
                    return null;
                }

                @Override
                public TipoUsuario getRowData(String rowKey) {
                    if (rowKey != null && !rowKey.isEmpty() && this.getWrappedData() != null) {
                        try {
                            Integer search = new Integer(rowKey);
                            for (TipoUsuario tu : (List<TipoUsuario>) getWrappedData()) {
                                if (tu.getIdTipoUsuario().compareTo(search) == 0) {
                                    return tu;
                                }
                            }
                        } catch (Exception e) {
                            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
                        }
                    }
                    return null;
                }

                @Override
                public List<TipoUsuario> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    try {
                        if (tipoUsrbd != null) {
                            this.setRowCount(tipoUsrbd.count());
                            tipoUsuarioList = tipoUsrbd.findRange(first, pageSize);
                        }
                    } catch (Exception e) {
                        System.out.println("Excepcion" + e.getMessage());
                    }
                    return tipoUsuarioList;
                }
            };

        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }

    }

    public void onRowSelect(SelectEvent event) {
        tipoUsuario = (TipoUsuario) event.getObject();

    }

    public TipoUsuario obtenerTipoUsuario() {
        return tipoUsrbd.find(tipoUsuario);
    }

    public void crearTipoUsuario() {
        tipoUsrbd.create(tipoUsuario);
        tipoUsuario = new TipoUsuario();
    }

    public List<TipoUsuario> getTipoUsuarioList() {
        return tipoUsuarioList;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public void iniciarTipoUsuario() {
        this.tipoUsuario = new TipoUsuario();
    }

    public void btnAgregarHandler(ActionEvent ae) {
        tipoUsrbd.create(tipoUsuario);
        this.iniciarTipoUsuario();
        LDM();
        FacesMessage mensaje = new FacesMessage();
        mensaje.setSeverity(FacesMessage.SEVERITY_INFO);
        mensaje.setSummary("EXITO");
        mensaje.setDetail("Se agrego un registro con éxito");
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }
}
